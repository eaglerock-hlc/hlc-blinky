FROM resin/raspberry-pi-alpine-python:3-slim
MAINTAINER EagleRock <peter.marks@gmail.com>

ADD blink.py ./

ENTRYPOINT [ "python", "./blink.py" ]
