Raspberry Pi LED Blinker Kubernetes Deployment
==============================================

A simple Docker container and Kubernetes deployment to cause the two
on-board Raspberry Pi LEDs to alternately blink.

Using the Docker container
--------------------------

To run a simple container, execute the following:

```
$ docker run -d -v /sys:/sys --name hlc-blinky eaglerock/hlc-blinky:latest
```

Deploying on Kubernetes
-----------------------

To create a Kubernetes deployment, execute the following:

```
$ kubectl apply -f ./hlc-blinky-kube.yml
```

The deployment starts with one replica. To test scaling of the deployment,
you can use commands such as the following:

```
$ kubectl scale --replias=3 deployment/hlc-blinky-dep
$ kubectl scale --replias=5 deployment/hlc-blinky-dep
$ kubectl scale --replias=0 deployment/hlc-blinky-dep
```
