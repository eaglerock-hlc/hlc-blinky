#!/usr/bin/python
import os
import sys
import atexit
import signal
from time import sleep

# blink.sh   - Simple script to alternately blink the red and green
#              on-board Raspberry Pi LEDs
# 2018-12-13 - Peter P. Marks
#
# Version History:
# v1.0 - Initial script - flashed red LED
# v1.1 - Alternately flashes red and green LEDs
# v1.2 - Streamlined flashing variables and functions
# v1.3 - Adjusted time logic
#
# LED devices:
# led0 - green LED (defaults to mmc0)
# led1 - red LED (defautls to default-on)

# Time delay variable between flashes
my_delay = 1.0

# Dictionary for LED trigger types
my_trigger = {
  "low": "mmc0",
  "high": "default-on"
}

# Dictionary for LED device IDs
my_led = {
  "green": "led0",
  "red": "led1"
}

def flick_light(my_level, my_color):
  '''
  Function to return OS system command to toggle a Pi LED:
  my_level = high or low, corresponding to default-on or mmc0, respectively
  my_color = green or red, corresponding to the LED device
  '''
  # Retrieving echo string, device name, and path to LED sys file
  my_echo = my_trigger.get(my_level)
  my_device = my_led.get(my_color)
  my_file = "/sys/class/leds/{}/trigger".format(my_device)

  # Returning OS command to run
  my_return = "echo '{}' | tee {}".format(my_echo, my_file)
  return my_return

def fix_lights():
  '''
  Simple function to ensure the LED is on before exit
  '''
  os.system("echo mmc0 | tee /sys/class/leds/led0/trigger")
  os.system("echo default-on | tee /sys/class/leds/led1/trigger")
  sys.exit(0)

# Register atexit and signal handlers to fix lights
atexit.register(fix_lights)
signal.signal(signal.SIGTERM, fix_lights) 
signal.signal(signal.SIGINT, fix_lights) 

while True:
  # First light configuration (reverse)
  os.system(flick_light("high", "green"))
  os.system(flick_light("low", "red"))
  sleep(my_delay)

  # Second light configuration (normal)
  os.system(flick_light("low", "green"))
  os.system(flick_light("high", "red"))
  sleep(my_delay)
